package tdd.fizzbuzz;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {
    @Test
    void should_say_Fizz_when_countOff_given_the_number_is_multiple_of_3() {
     //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String speak = fizzBuzz.countOff(3);

     //then
        assertEquals(speak, "Fizz");
    }
    @Test
    void should_say_Fizz_when_countOff_given_the_number_is_multiple_of_5() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String speak = fizzBuzz.countOff(5);

        //then
        assertEquals(speak, "Buzz");
    }
    @Test
    void should_say_Fizz_when_countOff_given_the_number_is_multiple_of_3_and_5() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String speak = fizzBuzz.countOff(15);

        //then
        assertEquals(speak, "FizzBuzz");
    }
    @Test
    void should_say_Fizz_when_countOff_given_the_number_is_multiple_of_7() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String speak = fizzBuzz.countOff(7);

        //then
        assertEquals(speak, "Whizz");
    }
    @Test
    void should_say_Fizz_when_countOff_given_the_number_is_multiple_of_normal_number() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String speak = fizzBuzz.countOff(1);

        //then
        assertEquals(speak, "1");
    }



}
